/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.server;

import com.simpchat.common.ChatMessage;
import com.simpchat.common.ChatMessageHandler;
import com.simpchat.common.ChatParticipiant;
import com.simpchat.common.ChatParticipiantImpl;
import java.io.IOException;
import java.net.Socket;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergii Puzyrov
 */
public class ChatServerTest {

    private static String TEST_START_MSG = "Test '%s' begin...";
    private static String TEST_END_SUCC_MSG = "Test '%s' end success";
    private static String TEST_END_FAIL_MSG = "Test '%s' end fail";
    private static String STEP_START_MSG = "%s begin...";
    private static String STEP_END_SUCC_MSG = "%s end success";
    private static String STEP_END_FAIL_MSG = "%s end fail";

    private static ChatServer instanceServer;
    private static ChatParticipiant chatParticipiant;

    public ChatServerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws IOException {
        instanceServer = ChatServerImpl.getInstance();
    }

    @AfterClass
    public static void tearDownClass() throws IOException {
        if (chatParticipiant != null && chatParticipiant.isActive()){
            chatParticipiant.turn(false);
        }
        
        if (instanceServer != null && instanceServer.isStarted()) {
            instanceServer.stop();
        }
    }

    @Before
    public void setUp() throws IOException {
        if (instanceServer != null && !instanceServer.isStarted()) {
            instanceServer.start();
        }
    }

    @After
    public void tearDown() throws IOException {
        if (chatParticipiant != null && chatParticipiant.isActive()){
            chatParticipiant.turn(false);
        }
        
        if (instanceServer != null && instanceServer.isStarted()) {
            instanceServer.stop();
        }
    }

    /**
     * Test of start method, of class ChatServer.
     */
    @Test
    public void testStart() throws Exception {
        System.out.println(String.format(TEST_START_MSG, "start"));
        assertTrue(String.format(TEST_END_FAIL_MSG, "start"), instanceServer.isStarted());
        System.out.println(String.format(TEST_END_SUCC_MSG, "start"));
    }

    /**
     * Test of stop method, of class ChatServer.
     */
    @Test
    public void testStop() throws Exception {
        System.out.println(String.format(TEST_START_MSG, "stop"));
        instanceServer.stop();
        assertFalse(String.format(TEST_END_FAIL_MSG, "stop"), instanceServer.isStarted());
        System.out.println(String.format(TEST_END_SUCC_MSG, "stop"));
    }

    /**
     * Test of addChatParticipiant method, of class ChatServer.
     */
    @Test
    public void testAddChatParticipiant() throws IOException {
        System.out.println(String.format(TEST_START_MSG, "addChatParticipiant"));
        String testNickName = "testNick1";
        System.out.println(String.format(STEP_START_MSG, "Add chat part"));
        chatParticipiant = new ChatParticipiantImpl(-1, testNickName,
                new ChatHandlerTestImpl(),
                new Socket("localhost", ChatServer.DEFAULT_CHAT_SERVER_PORT), null);
        System.out.println(String.format(STEP_END_SUCC_MSG, "Add chat part"));
        assertTrue(String.format(TEST_END_FAIL_MSG, "Add chat part"), instanceServer.addChatParticipiant(chatParticipiant));
        System.out.println(String.format(TEST_END_SUCC_MSG, "Add chat part"));
    }

    /**
     * Test of removeChatParticipiant method, of class ChatServer.
     */
    @Test
    public void testRemoveChatParticipiant() throws IOException {
        System.out.println(String.format(TEST_START_MSG, "removeChatParticipiant"));
        String testNickName = "testNick1";
        System.out.println(String.format(STEP_START_MSG, "Add chat part"));
        chatParticipiant = new ChatParticipiantImpl(-1, testNickName,
                new ChatHandlerTestImpl(),
                new Socket("localhost", ChatServer.DEFAULT_CHAT_SERVER_PORT), null);
        assertTrue(String.format(STEP_END_FAIL_MSG, "Add chat part"), instanceServer.addChatParticipiant(chatParticipiant));
        System.out.println(String.format(STEP_END_SUCC_MSG, "Add chat part"));
        
        System.out.println(String.format(STEP_START_MSG, "Find chat part"));
        ChatParticipiant chPart = instanceServer.findParticipiantByName(testNickName);
        assertTrue(String.format(STEP_END_FAIL_MSG, "Find chat part"), chPart != null);
        System.out.println(String.format(STEP_END_SUCC_MSG, "Find chat part"));
        
        assertTrue(String.format(TEST_END_FAIL_MSG, "findParticipiantByName"), instanceServer.removeChatParticipiant(chPart));
        System.out.println(String.format(TEST_END_SUCC_MSG, "removeChatParticipiant"));
    }


    /**
     * Test of findParticipiantByName method, of class ChatServer.
     */
    @Test
    public void testFindParticipiantByName() throws IOException {
        System.out.println(String.format(TEST_START_MSG, "findParticipiantByName"));
        String testNickName = "testNick1";
        System.out.println(String.format(STEP_START_MSG, "Add chat part"));
        chatParticipiant = new ChatParticipiantImpl(-1, testNickName,
                new ChatHandlerTestImpl(),
                new Socket("localhost", ChatServer.DEFAULT_CHAT_SERVER_PORT), null);
        assertTrue(String.format(STEP_END_FAIL_MSG, "Add chat part"), instanceServer.addChatParticipiant(chatParticipiant));
        System.out.println(String.format(STEP_END_SUCC_MSG, "Add chat part"));
        String addedName = instanceServer.findParticipiantByName(testNickName).getName();
        assertTrue(String.format(TEST_END_FAIL_MSG, "findParticipiantByName"), testNickName.equalsIgnoreCase(addedName));
        System.out.println(String.format(TEST_END_SUCC_MSG, "findParticipiantByName"));
    }
//
//    /**
//     * Test of addMessage method, of class ChatServer.
//     */
//    @Test
//    public void testAddMessage() {
//        System.out.println("addMessage");
//        ChatMessage msg = null;
//        ChatServer instance = new ChatServerImpl();
//        instance.addMessage(msg);
//    }
//
//    /**
//     * Test of removeMessage method, of class ChatServer.
//     */
//    @Test
//    public void testRemoveMessage() {
//        System.out.println("removeMessage");
//        ChatMessage msg = null;
//        ChatServer instance = new ChatServerImpl();
//        instance.removeMessage(msg);
//    }
//
//    /**
//     * Test of sendBroadcast method, of class ChatServer.
//     */
//    @Test
//    public void testSendBroadcast() {
//        System.out.println("sendBroadcast");
//        ChatMessage broadcastMsg = null;
//        ChatParticipiant[] excludeParts = null;
//        instance.sendBroadcast(broadcastMsg, excludeParts);
//    }

    private class ChatHandlerTestImpl implements ChatMessageHandler {

        @Override
        public void beforeProcessing() {
        }

        @Override
        public void processing(ChatMessage message) {
        }

        @Override
        public void afterProcessing() {
        }

    }
}
