/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.server;

import com.simpchat.common.ChatServiceMessages;
import com.simpchat.common.ChatMessageImpl;
import com.simpchat.common.ChatLogger;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import com.simpchat.common.ChatParticipiant;
import com.simpchat.common.ChatParticipiantImpl;
import com.simpchat.common.ServiceMessage;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import com.simpchat.common.ChatMessageHandler;
import java.io.Serializable;
import com.simpchat.common.ChatMessage;
import java.util.logging.Logger;

/**
 *
 * @author Sergii Puzyrov
 */
public class ChatServerImpl implements ChatServer, Runnable {

    public static String PATH_TO_CHAT_LOG_FILE = "chat" + File.separator + "chat_messages.txt";
    public static boolean LOGGING_CHAT_TO_FILE = true;

    private final int port;
    private ServerSocket servSocket;
    private Thread servThread;
    private final Map<Long, ChatParticipiant> participiants = new HashMap<>();
    private final List<ChatMessage> messages = new ArrayList<>();
    private final Random randIdGen = new Random();
    private final ChatLogger serverLogger = new ChatLogger(ChatServer.class, true);

    private static ChatServerImpl serverThread = null;

    private ChatServerImpl(int port) {
        this.port = port;
    }

    public static ChatServerImpl getInstance(int port) throws IOException {
        if (serverThread == null) {
            serverThread = new ChatServerImpl(port);
        } else if (port != serverThread.port) {
            serverThread.close();
            serverThread = new ChatServerImpl(port);
        }

        return serverThread;
    }

    public static ChatServerImpl getInstance() throws IOException {
        return getInstance(DEFAULT_CHAT_SERVER_PORT);
    }

    @Override
    public void start() throws IOException {
        serverLogger.entering(ChatServer.class.getSimpleName(), "Start");
        if (isStarted()) {
            serverLogger.log(Level.WARNING, "Server is running");
            return;
        }
        serverLogger.log(Level.INFO, "Creating server socket at port: " + this.port);
        int tryStartCount = 0;
        do {
            try {
                serverLogger.log(Level.INFO, "Try to create server socket at port: " + this.port);
                servSocket = new ServerSocket(this.port);
                serverLogger.log(Level.INFO, "Server socket at port: " + this.port + " was created success");
                break;
            } catch (IOException ex) {
                if (++tryStartCount >= MAX_TRY_START_COUNT) {
                    serverLogger.log(Level.SEVERE, "Server socket at port: " + this.port + " was created fail", ex);
                    throw ex;
                } else {
                    serverLogger.log(Level.WARNING, "Count trying " + tryStartCount, ex);
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                serverLogger.log(Level.SEVERE, null, ex);
            }
        } while (true);

        serverLogger.log(Level.INFO, "Create server thread");
        servThread = new Thread(this);
        serverLogger.log(Level.INFO, "Run server thread");
        servThread.start();

        serverLogger.exiting(ChatServer.class.getSimpleName(), "Start");
    }

    @Override
    public void stop() throws IOException {
        serverLogger.entering(ChatServer.class.getSimpleName(), "Stop");
        if (!isStarted()) {
            serverLogger.log(Level.WARNING, "Server is stopped");
            return;
        }
        serverLogger.log(Level.INFO, "Server closing start");
        close();
        serverLogger.log(Level.INFO, "Server closing finish");
        serverLogger.log(Level.INFO, "Join server thread start");
        servThread.interrupt();
        try {
            servThread.join(100);
        } catch (InterruptedException ex) {
            serverLogger.log(Level.SEVERE, null, ex);
        }
        serverLogger.log(Level.INFO, "Join server thread finish");
        serverLogger.exiting(ChatServer.class.getSimpleName(), "Stop");
    }

    public void close() throws IOException {
        serverLogger.entering(ChatServer.class.getSimpleName(), "Close");
        servSocket.close();
        synchronized (participiants) {
            for (Long id : participiants.keySet()) {
                ChatParticipiant chatParticipiant = participiants.get(id);
                if (chatParticipiant != null && chatParticipiant.isActive()) {
                    try {
                        chatParticipiant.turn(false);
                    } catch (IOException ex) {
                        serverLogger.log(Level.SEVERE,
                                ChatServer.class.getSimpleName() + " method close", ex);
                    }
                }
            }
            participiants.clear();
            messages.clear();
        }
        serverLogger.exiting(ChatServer.class.getSimpleName(), "Close");
    }

    @Override
    public boolean isStarted() {
        return servSocket != null && !servSocket.isClosed();
    }

    @Override
    public boolean addChatParticipiant(ChatParticipiant chatParticipiant) {
        serverLogger.entering(ChatServer.class.getSimpleName(), "addChatParticipiant");
        if (chatParticipiant == null) {
            serverLogger.log(Level.WARNING, "Try to register null chat participiant");
            return false;
        }
        synchronized (participiants) {
            serverLogger.log(Level.INFO, "Add participiant " + chatParticipiant + " to chat start");
            participiants.put(chatParticipiant.getId(), chatParticipiant);
            serverLogger.log(Level.INFO, "Add participiant " + chatParticipiant + " to chat finish");
            participiants.notifyAll();
        }
        String msgStr = String.format(ChatServiceMessages.ERROR_JOIN_CHAT_PART,
                chatParticipiant.getName());

        serverLogger.log(Level.INFO, "Sending broadcast message " + msgStr + " start");
        sendBroadcast(new ChatMessageImpl(new ServiceMessage(msgStr)), chatParticipiant);
        serverLogger.log(Level.INFO, "Sending broadcast message " + msgStr + " finish");

        serverLogger.exiting(ChatServer.class.getSimpleName(), "addChatParticipiant");
        return true;
    }

    @Override
    public boolean removeChatParticipiant(ChatParticipiant chatParticipiant) {
        serverLogger.entering(ChatServer.class.getSimpleName(), "removeChatParticipiant");
        if (chatParticipiant != null) {
            try {
                serverLogger.log(Level.INFO, "Stopping chat participant " + chatParticipiant + " start");
                chatParticipiant.turn(false);
                serverLogger.log(Level.INFO, "Stopping chat participant " + chatParticipiant + " finish");
                synchronized (participiants) {
                    serverLogger.log(Level.INFO, "Remove chat participant " + chatParticipiant + "  from list start");
                    participiants.remove(chatParticipiant.getId());
                    serverLogger.log(Level.INFO, "Remove chat participant " + chatParticipiant + "  from list finish");
                    participiants.notifyAll();
                }
                String msgStr = String.format(ChatServiceMessages.ERROR_LEAVE_CHAT_PART,
                        chatParticipiant.getName());
                serverLogger.log(Level.INFO, "Send broadcast message that chat participant " + chatParticipiant + "  is left start");
                sendBroadcast(new ChatMessageImpl(new ServiceMessage(msgStr)), chatParticipiant);
                serverLogger.log(Level.INFO, "Send broadcast message that chat participant " + chatParticipiant + "  is left finish");
                return true;
            } catch (IOException ex) {
                serverLogger.log(Level.SEVERE, null, ex);
                return false;
            }
        }
        serverLogger.exiting(ChatServer.class.getSimpleName(), "removeChatParticipiant");
        return false;
    }

    @Override
    public ChatParticipiant findParticipiantById(Long id) {
        serverLogger.entering(ChatServer.class.getSimpleName(), "findParticipiantById");
        ChatParticipiant existChatPart = null;
        synchronized (participiants) {
            existChatPart = participiants.get(id);
        }
        return existChatPart;
    }

    @Override
    public ChatParticipiant findParticipiantByName(String name) {
        ChatParticipiant existChatPart = null;
        if (participiants != null) {
            synchronized (participiants) {
                for (Long id : participiants.keySet()) {
                    String nm = participiants.get(id).getName();
                    if (nm != null && nm.equalsIgnoreCase(name)) {
                        existChatPart = participiants.get(id);
                        break;
                    }
                }
            }
        }

        return existChatPart;
    }

    private synchronized void logMessageToFile(ChatMessage msg, String fileName) {
        serverLogger.entering(ChatServer.class.getSimpleName(), "logMessageToFile");
        if (msg == null || fileName == null || fileName.isEmpty()) {
            return;
        }
        String path = fileName.substring(0, fileName.lastIndexOf(File.separator));
        if (path != null && !path.isEmpty() && !(new File(path).exists())) {
            new File(path).mkdirs();
        }

        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileName, true))) {
            String line = "[" + msg.getSender().getName() + "]:" + msg.getMessageBody() + "\n";
            out.write(line.getBytes());
        } catch (IOException ex) {
            serverLogger.log(Level.SEVERE, null, ex);
        }
        serverLogger.exiting(ChatServer.class.getSimpleName(), "logMessageToFile");
    }

    @Override
    public void addMessage(ChatMessage msg) {
        synchronized (messages) {
            serverLogger.entering(ChatServer.class.getSimpleName(), "addMessage");
            serverLogger.log(Level.INFO, "Add message to chat list");
            messages.add(msg);
            serverLogger.log(Level.INFO, "Send this message to all chat participiant", msg);
            sendBroadcast(msg);
            if (LOGGING_CHAT_TO_FILE) {
                serverLogger.log(Level.INFO, "Start writting message " + msg + " to file " + PATH_TO_CHAT_LOG_FILE);
                logMessageToFile(msg, PATH_TO_CHAT_LOG_FILE);
                serverLogger.log(Level.INFO, "Finish writting " + msg + " to file " + PATH_TO_CHAT_LOG_FILE);
            }
            serverLogger.exiting(ChatServer.class.getSimpleName(), "addMessage");
            messages.notifyAll();
        }
    }

    @Override
    public void removeMessage(ChatMessage msg) {
        if (messages.isEmpty()) {
            return;
        }
        synchronized (messages) {
            messages.remove(msg);
        }
    }

    @Override
    public void sendBroadcast(ChatMessage broadcastMsg, ChatParticipiant... excludeParts) {
        synchronized (participiants) {
            for (Long id : participiants.keySet()) {
                ChatParticipiant chatParticipiant = participiants.get(id);
                boolean continueSending = true;
                for (int i = 0; i < excludeParts.length && continueSending; ++i) {
                    continueSending = !excludeParts[i].equals(chatParticipiant);
                }
                if (chatParticipiant != null && chatParticipiant.isActive() && continueSending) {
                    try {
                        chatParticipiant.sendMessage(broadcastMsg);
                    } catch (IOException ex) {
                        serverLogger.log(Level.SEVERE, null, ex);
                    }
                }
            }
            participiants.notifyAll();
        }
    }

    private void registerParticipiant(Socket socket) throws IOException {
        serverLogger.entering(TestChatServer.class.getName(), "registerParticipiant");
        if (socket != null) {
            long id;
            //find unique id
            do {
                id = randIdGen.nextInt(MAX_COUNT_PARTICIPIANTS + 1) + 1;
            } while (findParticipiantById(id) != null);

            ChatParticipiantImpl chatPart
                    = new ChatParticipiantImpl(id, "", null, socket, serverLogger);

            ServerChatHandler chatHandler = new ServerChatHandler(chatPart);
            chatPart.setChatHandler(chatHandler);
            chatPart.turn(true);
            serverLogger.log(Level.INFO, "Created server chat {no name} participiant", chatPart);
        } else {
            serverLogger.log(Level.WARNING, "Uninitial socket");
        }
        serverLogger.exiting(TestChatServer.class.getName(), "registerParticipiant");
    }

    private boolean checkMaximumConnection(Socket socket) {
        serverLogger.entering(ChatServer.class.getSimpleName(), "checkMaximumConnection");
        boolean res;
        synchronized (participiants) {
            serverLogger.log(Level.INFO, "Check overflow connections start", "checkMaximumConnection");
            res = participiants.size() == MAX_COUNT_PARTICIPIANTS;
            if (res) {
                serverLogger.log(Level.WARNING, "Connection pool is overflow");
                try (PrintWriter pw = new PrintWriter(new ObjectOutputStream(socket.getOutputStream()))) {
                    pw.println(String.format(ChatServiceMessages.ERROR_MAX_CHAT_PARTS, participiants.size()));
                } catch (IOException ex) {
                    serverLogger.log(Level.SEVERE, null, ex);
                }
            }
            serverLogger.log(Level.INFO, "Check overflow connections finish", "checkMaximumConnection");
        }
        serverLogger.exiting(ChatServer.class.getSimpleName(), "checkMaximumConnection");
        return res;
    }

    private void handleNewConnection(Socket socket) throws IOException {
        serverLogger.entering(ChatServer.class.getSimpleName(), "handleNewConnection");
        if (!checkMaximumConnection(socket)) {
            serverLogger.log(Level.INFO, "Start to register a new participiant", "handleNewConnection");
            registerParticipiant(socket);
            serverLogger.log(Level.INFO, "Finish to register a new participiant", "handleNewConnection");
        }
        serverLogger.exiting(ChatServer.class.getSimpleName(), "handleNewConnection");
    }

    @Override
    public void run() {
        serverLogger.entering(ChatServer.class.getSimpleName(), "Server thread run");

        try {
            while (isStarted()) {
                serverLogger.log(Level.INFO, "Start listening", "run");
                Socket socket = servSocket.accept();
                serverLogger.log(Level.INFO, "Has a new connection", "run");
                serverLogger.log(Level.INFO, "Start handling", "run");
                handleNewConnection(socket);
                serverLogger.log(Level.INFO, "Finish handling", "run");
            }
        } catch (IOException ex) {
            serverLogger.log(Level.WARNING, "Stop listening after server socket close exception", "run");
        }

        serverLogger.log(Level.INFO, "Stop listening after server socket close exception", "run");
        serverLogger.exiting(ChatServer.class.getSimpleName(), "Server thread run");
    }

    private class ServerChatHandler implements ChatMessageHandler {

        private ChatParticipiant chatParticipiant;

        public ServerChatHandler(ChatParticipiant chatParticipiant) {
            this.chatParticipiant = chatParticipiant;
        }

        public ChatParticipiant getChatParticipiant() {
            return chatParticipiant;
        }

        @Override
        public void beforeProcessing() {
        }

        @Override
        public void processing(ChatMessage message) {
            serverLogger.entering(ServerChatHandler.class.getSimpleName(), "processing");
            if (message == null || chatParticipiant == null) {
                serverLogger.log(Level.WARNING, "Message and participiant must not be empty both");
                return;
            }
            serverLogger.log(Level.INFO, message.toString());
            Serializable msgBody = message.getMessageBody();
            if (chatParticipiant.getName().isEmpty()) {
                ((ChatParticipiantImpl) chatParticipiant).setName((String) msgBody);
                addChatParticipiant(chatParticipiant);
                synchronized (messages) {
                    serverLogger.log(Level.INFO, "Sending all chat for newcommer start");
                    for (ChatMessage msg : messages) {
                        try {
                            chatParticipiant.sendMessage(msg.getMessageBody(), msg.getSender());
                        } catch (IOException ex) {
                            serverLogger.log(Level.SEVERE, null, ex);
                        }
                    }
                    serverLogger.log(Level.INFO, "Sending all chat for newcommer end");
                    messages.notifyAll();
                }
            } else {
                addMessage(message);
            }
            serverLogger.exiting(ServerChatHandler.class.getSimpleName(), "processing");
        }

        @Override
        public void afterProcessing() {
            serverLogger.entering(ServerChatHandler.class.getSimpleName(), "afterProcessing");
            if (chatParticipiant != null) {
                removeChatParticipiant(chatParticipiant);
            }
            serverLogger.exiting(ServerChatHandler.class.getSimpleName(), "afterProcessing");
        }
    }

}
