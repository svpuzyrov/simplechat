/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.server;

import com.simpchat.common.ChatParticipiant;
import java.io.IOException;
import java.io.Serializable;
import com.simpchat.common.ChatMessage;

/**
 * Interface <code>ChatServer</code> has basic functionality for chat server
 * @author Sergii Puzyrov
 */
public interface ChatServer extends Serializable{
    int MAX_COUNT_PARTICIPIANTS = 10000;
    int WAIT_TIME_OUT = 10;
    int DEFAULT_CHAT_SERVER_PORT = 54030;
    int MAX_TRY_START_COUNT = 10;
    /**
     * Start server
     * @throws java.io.IOException
     */
    void start() throws IOException;
    /**
     * Stop server
     * @throws java.io.IOException
     */
    void stop() throws IOException;
    /**
     * Check server status
     * @return true if server started
     */
    boolean isStarted();
    /**
     * Add participiant to chat
     * @param chatParticipiant
     * @return true if register in chat was success
     */
    boolean addChatParticipiant(ChatParticipiant chatParticipiant);
    /**
     * Remove participiant to chat
     * @param chatParticipiant
     * @return true if deleting from chat was success
     */
    boolean removeChatParticipiant(ChatParticipiant chatParticipiant);
    /**
     * Find participiant in chat by id
     * @param id
     * @return reference to chat participiant
     */
    ChatParticipiant findParticipiantById(Long id);
    /**
     * Find participiant in chat by name
     * @param name
     * @return reference to chat participiant
     */
    ChatParticipiant findParticipiantByName(String name);
    /**
     * Add message to chat
     * @param msg
     */
    void addMessage(ChatMessage msg);
    /**
     * Remove message from chat
     * @param msg
     */
    void removeMessage(ChatMessage msg);
    /**
     * Send message to all participiant of chat
     * @param broadcastMsg message to sending
     * @param excludeParts participiants which don't get this message
     */
    void sendBroadcast(ChatMessage broadcastMsg, ChatParticipiant... excludeParts);
}
