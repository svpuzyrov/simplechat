/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.server;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sergii Puzyrov
 */
public class TestChatServer {
    public static int DEFAULT_SERVER_PORT = 54030;
    private ChatServer chatServer;
    private int port;

    public TestChatServer(int port) throws IOException {
        this.port = port;
        this.chatServer = ChatServerImpl.getInstance(port);
    }
    
    public ChatServer getChatServer(){
        return chatServer;
    }
    
    public static void main(String[] args) {
        int port = DEFAULT_SERVER_PORT;
        if(args.length > 0){
            String[] spArgs = args[0].split(":");
            if (spArgs != null && spArgs.length > 1 && spArgs[0].equalsIgnoreCase("-port")){
                port = Integer.parseInt(spArgs[1]);
            }
        }
        TestChatServer server = null;
        try(Scanner in = new Scanner(System.in)) {
            server = new TestChatServer(port);
            
            System.out.println("Enter server command (Start - S, Stop - T, Exit - Q)");
            while(true){
                String cmd = in.nextLine();
                if (cmd.equalsIgnoreCase("Q")){
                    break;
                }
                if (cmd.equalsIgnoreCase("S") && !server.getChatServer().isStarted()){
                    server.getChatServer().start();
                    System.out.println("Server started");
                }
                
                if (cmd.equalsIgnoreCase("T") && server.getChatServer().isStarted()){
                    server.getChatServer().stop();
                    System.out.println("Server stopped");
                }
                Thread.sleep(100);
            }
            
            server.getChatServer().stop();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(TestChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
