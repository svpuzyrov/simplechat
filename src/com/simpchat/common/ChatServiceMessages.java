/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

/**
 * Class <code>ChatServiceMessages</code> has static constants for service messages
 * @author Sergii Puzyrov
 */
public class ChatServiceMessages {
    public static String ERROR_MAX_CHAT_PARTS = "Maximum count of participiant can not be greater then %d";
    public static String ERROR_JOIN_CHAT_PART = "Participiant [%s] has joined to this chat";
    public static String ERROR_LEAVE_CHAT_PART = "Participiant [%s] has left from this chat";
}
