/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.Serializable;

/**
 * Class <code>ServiceMessage</code> represents of a service messages from server
 * @author Sergii Puzyrov
 */
public class ServiceMessage implements Serializable{
    private String message;

    public ServiceMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return message;
    }
    
    
}
