/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author svpuzyrov
 */
public class ChatParticipiantImpl implements ChatParticipiant, Runnable {

    private String name;
    private long id;
    private transient ChatMessageHandler chatHandler;
    private transient ChatLogger chatLogger;

    protected transient Socket socket;
    protected transient Thread partThread;
    protected transient ObjectInputStream oIn;
    protected transient ObjectOutputStream oOut;

    public ChatParticipiantImpl(long id, String name,
            ChatMessageHandler chatHandler, Socket socket, ChatLogger chatLogger) {
        this.id = id;
        this.name = name;
        this.chatHandler = chatHandler;
        this.socket = socket;
        this.chatLogger = chatLogger;
    }

    protected void openStreams() throws IOException {
        entering("openStreams");
        logging(Level.INFO, "Open streams start");
        oOut = new ObjectOutputStream(getSocket().getOutputStream());
        oIn = new ObjectInputStream(getSocket().getInputStream());
        logging(Level.INFO, "Open streams finish");
        exiting("openStreams");
    }

    protected void closeStreams() throws IOException {
        entering("closeStreams");
        logging(Level.INFO, "Close streams start");
        if (oIn != null) {
            oIn.close();
            oIn = null;
        }
        if (oOut != null) {
            oOut.close();
            oOut = null;
        }
        logging(Level.INFO, "Close streams finish");
        exiting("closeStreams");
    }
    
    private void entering(String methodName){
        if (chatLogger != null){
            chatLogger.entering(ChatParticipiant.class.getSimpleName(), methodName);
        }
    }
    
    private void exiting(String methodName){
        if (chatLogger != null){
            chatLogger.exiting(ChatParticipiant.class.getSimpleName(), methodName);
        }
    }
    
    private void logging(Level level, String message){
        if (chatLogger != null){
            chatLogger.log(level, message);
        }
    }
    
    private void logging(Level level, String message, Object param){
        if (chatLogger != null){
            chatLogger.log(level, message, param);
        }
    }
    
    private void logging(Level level, String message, Throwable ex){
        if (chatLogger != null){
            chatLogger.log(level, message, ex);
        }
    }

    @Override
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public ChatMessageHandler getChatHandler() {
        return chatHandler;
    }

    public void setChatHandler(ChatMessageHandler chatHandler) {
        this.chatHandler = chatHandler;
    }

    @Override
    public boolean isActive() {
        return getSocket() != null && !getSocket().isClosed()
                && partThread != null && partThread.isAlive();
    }
    
    @Override
    public Socket getSocket() {
        return this.socket;
    }
    
    @Override
    public void sendMessage(ChatMessage message) throws IOException {
        entering("sendMessage");
        if (oOut != null && isActive()) {
            logging(Level.INFO, "Sending message {" + message + "} start");
            oOut.writeObject(message);
            logging(Level.INFO, "Sending message {" + message + "} finish");
        }
        exiting("sendMessage");
    }
    
    @Override
    public void sendMessage(Serializable messageBody) throws IOException {
        sendMessage(messageBody, this);
    }
    
    @Override
    public void sendMessage(Serializable messageBody,
                            ChatParticipiant chatParticipiantFrom) throws IOException {
        entering("sendMessage");
        if (oOut != null && isActive()) {
            logging(Level.INFO, "Sending message {" + messageBody + "} from " +
                    chatParticipiantFrom + " start");
            oOut.writeObject(new ChatMessageImpl(messageBody, chatParticipiantFrom));
            logging(Level.INFO, "Sending message {" + messageBody + "} from " +
                    chatParticipiantFrom + " finish");
        }
        exiting("sendMessage");
    }

    @Override
    public ChatMessage readMessage() throws IOException {
        entering("readMessage");
        ChatMessage data = null;
        if (oIn != null && isActive()) {
            try {
                logging(Level.INFO, "Reading message start");
                data = (ChatMessage) oIn.readObject();
                logging(Level.INFO, "Reading message {" + data +"} finish");
            } catch (ClassNotFoundException ex) {
                logging(Level.SEVERE, null, ex);
            }
        }
        exiting("readMessage");
        return data;
    }

    @Override
    public void turn(boolean onOff) throws IOException {
        entering("turn");
        if (onOff && !isActive()) {
            logging(Level.INFO, "Turn on start");
            openStreams();
            logging(Level.INFO, "Chat part thread start");
            partThread = new Thread(this);
            partThread.start();
            logging(Level.INFO, "Chat part thread finish");
            logging(Level.INFO, "Turn on finish");
        } else {
            logging(Level.INFO, "Turn off start");
            closeStreams();
            logging(Level.INFO, "Close socket start");
            if (getSocket() != null) {
                getSocket().close();
            }
            logging(Level.INFO, "Close socket finish");
            logging(Level.INFO, "Thread interrupt start");
            if (partThread != null) {
                try {
                    partThread.join(10);
                } catch (InterruptedException ex) {
                    logging(Level.SEVERE, null, ex);
                }
                partThread = null;
            }
            logging(Level.INFO, "Thread interrupt finish");
            logging(Level.INFO, "Turn off finish");
        }
    }

    @Override
    public void run() {
        if (chatHandler != null){
            chatHandler.beforeProcessing();
        }
        
        while (chatHandler != null && isActive()) {
            try {
                chatHandler.processing(readMessage());
            } catch (IOException ex) {
                break;
            }
        }
        
        if (chatHandler != null){
            chatHandler.afterProcessing();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChatParticipiantImpl other = (ChatParticipiantImpl) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ChatParticipiant{" + id + ", " + name + '}';
    }

    

}
