/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.Serializable;

/**
 * Interface <code>ChatMessage</code> provides abstract message in chat
 * <p>This message has two parts
 * <ul>
 *  <li>Sender, that represents the participiant of chat and obtains by <code>getSender</code></li>
 *  <li>Body, that represents information message of chat and obtains by <code>getMessageBody</code></li>
 * </ul>
 * <p>If sender is null, it means a service message from server
 * @author Sergii Puzyrov
 */
public interface ChatMessage extends Serializable{
    /**
     * @return <code>ChatParticipiant</code> object, which sent this message
     */
    ChatParticipiant getSender();
    /**
     * @return any object, which can be interpreted as message
     */
    Serializable getMessageBody();
}
