/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

/**
 * <p>Interface <code>ChatMessageHandler</code> is nessesary for any implementation of
 * <code>ChatParticipiant</code> interface
 * @author Sergii Puzyrov
 */
public interface ChatMessageHandler {
    void beforeProcessing();
    void processing(ChatMessage message);
    void afterProcessing();
}
