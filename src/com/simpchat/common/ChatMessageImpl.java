/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.Serializable;
import java.util.Objects;
/**
 * @author Sergii Puzyrov
 * <p>Class <code>ChatMessageImpl</code> is implementation of the <code>ChatMessage</code>
 */
public class ChatMessageImpl implements ChatMessage {

    private Serializable message;
    private ChatParticipiant chatPart;

    public ChatMessageImpl(Serializable message, ChatParticipiant chatPart) {
        this.message = message;
        this.chatPart = chatPart;
    }
    
    public ChatMessageImpl(Serializable message) {
        this(message, null);
    }

    @Override
    public ChatParticipiant getSender() {
        return chatPart;
    }

    @Override
    public Serializable getMessageBody() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChatMessageImpl other = (ChatMessageImpl) obj;
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        if (!Objects.equals(this.chatPart, other.chatPart)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Message{"  + message + '}';
    }

    

}
