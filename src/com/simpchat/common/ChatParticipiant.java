/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

/**
 *
 * @author Sergii Puzyrov
 */
public interface ChatParticipiant extends Serializable{
    long getId();
    String getName();
    Socket getSocket();
    void turn(boolean onOff) throws IOException;
    boolean isActive();
    void sendMessage(ChatMessage message) throws IOException;
    void sendMessage(Serializable message) throws IOException;
    void sendMessage(Serializable message, ChatParticipiant chatParticipiantFrom) throws IOException;
    ChatMessage readMessage() throws IOException;
}
