/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.common;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author svpuzyrov
 * <p>Class <code>ChatLogger</code> is adapter for standart <code>Logger</code> class
 */
public class ChatLogger {

    private String className;
    private Logger logger;
    private FileHandler fileHandler;
    private boolean enableLogging = true;

    public ChatLogger(Class<?> classInstance, boolean enableLogging) {
        this.className = classInstance.getSimpleName();
        logger = Logger.getLogger(classInstance.getName());
        setEnableLogging(enableLogging);
        logger.setUseParentHandlers(false);
    }

    public boolean isEnableLogging() {
        return enableLogging;
    }

    public void setEnableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
        if (this.enableLogging) {
            try {
                fileHandler = new FileHandler(className + ".log", 1024 * 64, 20, false);
                fileHandler.setFormatter(new SimpleFormatter());
                logger.addHandler(fileHandler);

            } catch (IOException | SecurityException ex) {
                log(Level.SEVERE, ChatLogger.class.getName(), ex);
            }
        } else if (fileHandler != null) {
            logger.removeHandler(fileHandler);
        }
    }

    public void entering(String srcClass, String classMethod) {
        if (enableLogging) {
            logger.entering(srcClass, classMethod);
        }
    }

    public void entering(String srcClass, String classMethod, Object param) {
        if (enableLogging) {
            logger.entering(srcClass, classMethod, param);
        }
    }

    public void exiting(String srcClass, String classMethod) {
        if (enableLogging) {
            logger.exiting(srcClass, classMethod);
        }
    }

    public void exiting(String srcClass, String classMethod, Object param) {
        if (enableLogging) {
            logger.exiting(srcClass, classMethod, param);
        }
    }

    public void info(String msg, Object param) {
        if (enableLogging) {
            logger.log(Level.INFO, msg, param);
        }
    }

    public void severe(String msg, Object param) {
        if (enableLogging) {
            logger.log(Level.SEVERE, msg, param);
        }
    }

    public void warn(String msg, Object param) {
        if (enableLogging) {
            logger.log(Level.WARNING, msg, param);
        }
    }

    public void log(Level level, String msg) {
        if (enableLogging) {
            logger.log(level, msg);
        }
    }

    public void log(Level level, String msg, Object param) {
        if (enableLogging) {
            logger.log(level, msg, param);
        }
    }

    public void log(Level level, String msg, Throwable ex) {
        if (enableLogging) {
            logger.log(level, msg, ex);
        }
    }
}
