/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.client;

import com.simpchat.common.ChatParticipiant;
import com.simpchat.common.ChatParticipiantImpl;
import com.simpchat.common.ServiceMessage;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.Scanner;
import com.simpchat.common.ChatMessageHandler;
import com.simpchat.common.ChatMessage;

/**
 *
 * @author Admin
 */
public class TestClient {

    public static String DEFAULT_HOST = "127.0.0.1";
    public static int DEFAULT_PORT = 54030;

    private static class ClientChatHandler implements ChatMessageHandler {

        @Override
        public void beforeProcessing() {
        }

        @Override
        public void processing(ChatMessage message) {
            if (message != null) {
                ChatParticipiant chatPart = message.getSender();
                Serializable msgBody = message.getMessageBody();
                
                if (chatPart == null){
                    System.out.print("[Service message]:");
                }
                else{
                    System.out.print("[" + chatPart.getName() + "]:");
                }
                
                System.out.println(msgBody);                
            }
        }

        @Override
        public void afterProcessing() {
        }

    }

    public static void main(String[] args) {
        String host = DEFAULT_HOST;
        int port = DEFAULT_PORT;
        if (args.length > 0) {
            String[] argParts = args[0].split(":");
            if (argParts != null && argParts.length > 1) {
                host = argParts[0];
                port = Integer.parseInt(argParts[1]);
            }
        }
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter name: ");
            String name = scanner.nextLine();
            ChatParticipiant clientChat = new ChatParticipiantImpl(-1, name,
                    new ClientChatHandler(), new Socket(host, port), null);

            clientChat.turn(true);
            System.out.println("Connection to server " + host + ":" + port + " was successfull");
            System.out.println("Type \"exit\" | Q | q  to finish client");
            clientChat.sendMessage(name);
            while (clientChat.isActive()) {
                String line = scanner.nextLine();
                if (!line.isEmpty()) {
                    if (line.equalsIgnoreCase("exit") || line.equalsIgnoreCase("q")) {
                        clientChat.turn(false);
                    } else {
                        clientChat.sendMessage(line);
                    }
                }
            }
            System.out.println("Disconnect from server " + host + ":" + port);

        } catch (IOException ex) {
            System.out.println("Connection to server " + host + ":" + port + " was refused");
        }

    }

}
