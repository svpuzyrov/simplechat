/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpchat.client;

import com.simpchat.common.ChatMessage;
import com.simpchat.common.ChatMessageHandler;
import com.simpchat.common.ChatParticipiant;
import com.simpchat.common.ChatParticipiantImpl;
import com.simpchat.common.ServiceMessage;
import com.simpchat.server.ChatServer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Admin
 */
public class TestChatClientGUI extends JFrame {

    public static int DEFAULT_WIDTH = 400;
    public static int DEFAULT_HEIGHT = 640;

    private JPanel mainPanel;
    private JPanel connectPanel;
    private JLabel hostLabel;
    private JTextField hostTextField;
    private JButton connectButton;

    private JPanel messagePanel;
    private JLabel messageLabel;
    private JTextField messageTextField;
    private JButton sendMessageButton;

    private JPanel chatPanel;
    private JList<String> chatList;

    private ChatParticipiant chatParticipiant;

    public TestChatClientGUI(String title) throws HeadlessException {
        super(title);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocation(dim.width / 2 - getWidth() / 2, dim.height / 2 - getHeight() / 2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        setVisible(true);
    }

    private void initComponents() {
        //add(mainPanel = new JPanel());
        //mainPanel
        add(connectPanel = new JPanel(new GridLayout(1, 3)), BorderLayout.NORTH);
        connectPanel.add(hostLabel = new JLabel("host:port"));
        connectPanel.add(hostTextField
                = new JTextField("localhot:" + ChatServer.DEFAULT_CHAT_SERVER_PORT));
        connectPanel.add(connectButton = new JButton());
        connectButton.setAction(new ConnectHandler("Connect", connectButton));
        //mainPanel.
        add(messagePanel = new JPanel(new GridLayout(1, 3)), BorderLayout.CENTER);
        messagePanel.add(messageLabel = new JLabel("Message"));
        messagePanel.add(messageTextField = new JTextField());
        messagePanel.add(sendMessageButton = new JButton(new AbstractAction("Send") {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (chatParticipiant != null && chatParticipiant.isActive()) {
                    try {
                        chatParticipiant.sendMessage(messageTextField.getText(), chatParticipiant);
                    } catch (IOException ex) {
                        simpleExceptionHandler(ex);
                    }
                }
                messageTextField.setText("");
            }
        }));
        messagePanel.setVisible(false);

        //mainPanel.
        add(chatPanel = new JPanel(), BorderLayout.SOUTH);
        chatPanel.add(new JScrollPane(chatList = new JList<>(new DefaultListModel<String>())));

        pack();
    }

    private void simpleExceptionHandler(Exception ex) {
        JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }

    private class ConnectHandler extends AbstractAction {

        private JButton connectButton;

        public ConnectHandler(String name, JButton connectButton) {
            super(name);
            this.connectButton = connectButton;
        }

        public JButton getConnectButton() {
            return connectButton;
        }

        public void setConnectButton(JButton connectButton) {
            this.connectButton = connectButton;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (chatParticipiant == null || !chatParticipiant.isActive()) {
                if (hostTextField.getText().isEmpty()) {
                    return;
                }
                String hostName = "localhost";
                int port = ChatServer.DEFAULT_CHAT_SERVER_PORT;
                String[] hostPort = hostTextField.getText().split(":");
                if (hostPort.length > 0) {
                    if (hostPort.length > 1) {
                        port = Integer.parseInt(hostPort[1]);
                    }
                }
                String nickName = JOptionPane.showInputDialog("Enter your nick", "");
                if (nickName == null || nickName.isEmpty()) {
                    JOptionPane.showMessageDialog(TestChatClientGUI.this, "Your nick must not be empty",
                            "Ivalid nick", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    chatParticipiant = new ChatParticipiantImpl(-1, nickName,
                            new ChatClientMessageHandler(),
                            new Socket(hostName, port), null);
                    chatParticipiant.turn(true);
                    chatParticipiant.sendMessage(nickName);
                    connectButton.setText("Disconnect");
                    messagePanel.setVisible(true);
                } catch (IOException ex) {
                    simpleExceptionHandler(ex);
                }
            } else {
                try {
                    chatParticipiant.turn(false);
                    connectButton.setText("Connect");
                    messagePanel.setVisible(false);
                } catch (IOException ex) {
                    simpleExceptionHandler(ex);
                }
            }

        }

    }

    private class ChatClientMessageHandler implements ChatMessageHandler {

        @Override
        public void beforeProcessing() {
        }

        @Override
        public void processing(ChatMessage message) {
            if (message == null) {
                return;
            }
            ChatParticipiant chatPart = message.getSender();
            Serializable messageBody = message.getMessageBody();
            String messageLine = null;
            if (chatPart == null && (messageBody instanceof ServiceMessage)) {
                messageLine = "[Service message]:" + ((ServiceMessage) messageBody).getMessage();
            } else {
                messageLine = "[" + chatPart.getName() + "]:"
                        + (String) messageBody;
            }
            DefaultListModel<String> model = (DefaultListModel<String>) chatList.getModel();
            if (model == null) {
                model = new DefaultListModel<String>();
            }
            model.addElement(messageLine);
            chatList.setModel(model);
        }

        @Override
        public void afterProcessing() {
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TestChatClientGUI("Simple chat");
            }
        });
    }
}
